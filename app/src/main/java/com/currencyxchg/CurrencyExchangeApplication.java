package com.currencyxchg;

import android.app.Application;
import com.currencyxchg.di.AppComponent;
import com.currencyxchg.di.AppModule;
import com.currencyxchg.di.DaggerAppComponent;

public class CurrencyExchangeApplication extends Application {

  private AppComponent component;

  @Override public void onCreate() {
    super.onCreate();

    component = createComponent();
  }

  protected AppComponent createComponent() {
    return DaggerAppComponent.builder().appModule(new AppModule(this)).build();
  }

  public AppComponent getComponent() {
    return component;
  }
}
