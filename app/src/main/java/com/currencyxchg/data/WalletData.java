package com.currencyxchg.data;

import android.os.Parcel;
import android.os.Parcelable;
import java.math.BigDecimal;

/**
 * An object to interact between DTOs and UI
 *
 * The main purpose is to have separate abstraction of DTOs, because DTOs may be changed very
 * frequently
 */
public class WalletData implements Parcelable {

  public static final Parcelable.Creator<WalletData> CREATOR =
      new Parcelable.Creator<WalletData>() {
        @Override public WalletData createFromParcel(Parcel source) {
          return new WalletData(source);
        }

        @Override public WalletData[] newArray(int size) {
          return new WalletData[size];
        }
      };
  private String currency;
  private BigDecimal amount = BigDecimal.ZERO;

  public WalletData(String currency) {
    this.currency = currency;
  }

  public WalletData(String currency, BigDecimal amount) {
    this.currency = currency;
    this.amount = amount;
  }

  protected WalletData(Parcel in) {
    this.currency = in.readString();
    this.amount = (BigDecimal) in.readSerializable();
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  @Override public String toString() {
    return "WalletData{" +
        "currency='" + currency + '\'' +
        ", amount=" + amount +
        '}';
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.currency);
    dest.writeSerializable(this.amount);
  }
}
