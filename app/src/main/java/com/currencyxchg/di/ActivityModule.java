package com.currencyxchg.di;

import com.currencyxchg.annotations.PerActivity;
import com.currencyxchg.ui.BaseActivity;
import dagger.Module;
import dagger.Provides;

/**
 * All activity dependencies (with fragment scope) must be provided here
 */
@Module public class ActivityModule {
  private BaseActivity activity;

  public ActivityModule(BaseActivity activity) {
    this.activity = activity;
  }

  @PerActivity @Provides BaseActivity provideActivity() {
    return activity;
  }
}
