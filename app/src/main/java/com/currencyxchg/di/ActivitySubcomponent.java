package com.currencyxchg.di;

import com.currencyxchg.annotations.PerActivity;
import dagger.Subcomponent;

@PerActivity @Subcomponent(modules = ActivityModule.class)
public interface ActivitySubcomponent {

  FragmentSubcomponent fragmentSubcomponent(FragmentModule fragmentModule);
}
