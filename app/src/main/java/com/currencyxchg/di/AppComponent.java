package com.currencyxchg.di;

import com.currencyxchg.annotations.PerApplication;
import com.currencyxchg.repository.FxRepository;
import dagger.Component;

@PerApplication @Component(modules = AppModule.class) public interface AppComponent {

  FxRepository fxRepository();

  ActivitySubcomponent activitySubcomponent(ActivityModule activityModule);
}
