package com.currencyxchg.di;

import android.app.Application;
import com.currencyxchg.BuildConfig;
import com.currencyxchg.annotations.IOSched;
import com.currencyxchg.annotations.PerApplication;
import com.currencyxchg.annotations.UISched;
import com.currencyxchg.net.EcbApi;
import com.currencyxchg.repository.EcbFxRepository;
import com.currencyxchg.repository.FxRepository;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import dagger.Module;
import dagger.Provides;
import net.ypresto.timbertreeutils.CrashlyticsLogExceptionTree;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

@Module public class AppModule {

  private final Application application;

  public AppModule(Application application) {
    this.application = application;

    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    } else {
      Timber.plant(new CrashlyticsLogExceptionTree());
    }
  }

  @PerApplication @Provides public Application provideApplication() {
    return application;
  }

  @PerApplication @IOSched @Provides public Scheduler provideIOScheduler() {
    return Schedulers.io();
  }

  @PerApplication @UISched @Provides public Scheduler provideUIScheduler() {
    return AndroidSchedulers.mainThread();
  }

  @PerApplication @Provides public EcbApi provideApi(Application application) {
    Retrofit.Builder builder = new Retrofit.Builder().baseUrl(BuildConfig.API_BASE_URL)
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .addConverterFactory(SimpleXmlConverterFactory.create());

    if (BuildConfig.DEBUG) {
      Stetho.initializeWithDefaults(application);

      builder.client(
          new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build());
    }

    return builder.build().create(EcbApi.class);
  }

  @PerApplication @Provides public FxRepository provideFxRepository(EcbApi api,
      @UISched Scheduler uiScheduler, @IOSched Scheduler ioScheduler) {
    return new EcbFxRepository(api, uiScheduler, ioScheduler);
  }
}
