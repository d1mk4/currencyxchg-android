package com.currencyxchg.di;

import com.currencyxchg.annotations.PerFragment;
import com.currencyxchg.ui.BaseFragment;
import dagger.Module;
import dagger.Provides;

/**
 * All fragment dependencies (with fragment scope) must be provided here
 */
@Module public class FragmentModule {
  private BaseFragment fragment;

  public FragmentModule(BaseFragment fragment) {
    this.fragment = fragment;
  }

  @PerFragment @Provides BaseFragment provideFragment() {
    return fragment;
  }
}
