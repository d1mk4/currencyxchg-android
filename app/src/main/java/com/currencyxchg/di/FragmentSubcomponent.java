package com.currencyxchg.di;

import com.currencyxchg.annotations.PerFragment;
import com.currencyxchg.ui.ExchangeFragment;
import dagger.Subcomponent;

@PerFragment @Subcomponent(modules = FragmentModule.class)
public interface FragmentSubcomponent {

  void inject(ExchangeFragment fragment);
}
