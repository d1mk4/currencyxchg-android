package com.currencyxchg.di;

import android.app.Application;
import com.currencyxchg.BuildConfig;
import com.currencyxchg.annotations.PerApplication;
import com.currencyxchg.net.EcbApi;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Our network config
 */
@Module public class NetModule {

  @PerApplication @Provides EcbApi provideApi(Application application) {
    Retrofit.Builder builder = new Retrofit.Builder().baseUrl(BuildConfig.API_BASE_URL)
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .addConverterFactory(SimpleXmlConverterFactory.create());

    if (BuildConfig.DEBUG) {
      // Provide convenient way to debug network
      Stetho.initializeWithDefaults(application);

      builder.client(
          new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build());
    }

    return builder.build().create(EcbApi.class);
  }
}
