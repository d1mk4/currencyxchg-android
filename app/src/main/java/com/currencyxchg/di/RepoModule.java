package com.currencyxchg.di;

import com.currencyxchg.annotations.IOSched;
import com.currencyxchg.annotations.PerApplication;
import com.currencyxchg.annotations.UISched;
import com.currencyxchg.net.EcbApi;
import com.currencyxchg.repository.EcbFxRepository;
import com.currencyxchg.repository.FxRepository;
import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

/**
 * Repositories module where we provide specialized repositories for whole app
 *
 * It's just a config for sources of data
 */
@Module public class RepoModule {

  @PerApplication @Provides FxRepository provideFxRepository(EcbApi api,
      @UISched Scheduler uiScheduler, @IOSched Scheduler ioScheduler) {
    return new EcbFxRepository(api, uiScheduler, ioScheduler);
  }
}
