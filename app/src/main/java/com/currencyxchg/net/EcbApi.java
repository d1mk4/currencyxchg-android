package com.currencyxchg.net;

import com.currencyxchg.net.dto.RatesEnvelope;
import retrofit2.http.GET;
import rx.Observable;

/**
 * European Central Bank API to fetch currency rates
 */
public interface EcbApi {
  @GET("/stats/eurofxref/eurofxref-daily.xml") Observable<RatesEnvelope> getEuroFxDaily();
}
