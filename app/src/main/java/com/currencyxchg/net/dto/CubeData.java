package com.currencyxchg.net.dto;

import org.simpleframework.xml.Element;

/**
 * The second level of ECB XML protocol
 */
public class CubeData {

  @Element(name = "Cube") private CubeList cubeList;

  public CubeList getCubeList() {
    return cubeList;
  }

  public boolean isEmpty() {
    return cubeList == null || cubeList.getRatesMap() == null || cubeList.getRatesMap().isEmpty();
  }

  @Override public String toString() {
    return "CubeData{" +
        "cubeList=" + cubeList +
        '}';
  }
}
