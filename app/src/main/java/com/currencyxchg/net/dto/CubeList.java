package com.currencyxchg.net.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

/**
 * A list of currency rates in ECB XML protocol
 */
public class CubeList {

  @Attribute(name = "time") private String time;

  @ElementList(inline = true) private List<CurrencyRate> ratesList;

  public String getTime() {
    return time;
  }

  public Map<String, CurrencyRate> getRatesMap() {
    Map<String, CurrencyRate> currenciesMap = new HashMap<>();

    for (CurrencyRate datum : ratesList) {
      currenciesMap.put(datum.getCurrency(), datum);
    }

    return currenciesMap;
  }

  @Override public String toString() {
    return "CubeList{" +
        "time='" + time + '\'' +
        ", ratesList=" + ratesList +
        '}';
  }
}
