package com.currencyxchg.net.dto;

import android.os.Parcel;
import android.os.Parcelable;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Currency rate item
 *
 * I use parcelable to provide saving this item in Android's bundle
 */
@Root(name = "Cube") public class CurrencyRate implements Parcelable {

  public static final Parcelable.Creator<CurrencyRate> CREATOR =
      new Parcelable.Creator<CurrencyRate>() {
        @Override public CurrencyRate createFromParcel(Parcel source) {
          return new CurrencyRate(source);
        }

        @Override public CurrencyRate[] newArray(int size) {
          return new CurrencyRate[size];
        }
      };
  @Attribute(name = "currency") private String currency;
  @Attribute(name = "rate") private float rate;

  public CurrencyRate() {
  }

  public CurrencyRate(String currency, float rate) {
    this.currency = currency;
    this.rate = rate;
  }

  protected CurrencyRate(Parcel in) {
    this.currency = in.readString();
    this.rate = in.readFloat();
  }

  public String getCurrency() {
    return currency;
  }

  public float getRate() {
    return rate;
  }

  @Override public String toString() {
    return "CubeDatum{" +
        "currency='" + currency + '\'' +
        ", rate=" + rate +
        '}';
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.currency);
    dest.writeFloat(this.rate);
  }
}
