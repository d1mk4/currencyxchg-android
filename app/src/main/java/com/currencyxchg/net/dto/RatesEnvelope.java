package com.currencyxchg.net.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * The first level of ECB XML protocol
 */
@Root(name = "gesmes:Envelope", strict = false) public class RatesEnvelope {

  @Element(name = "Cube") private CubeData dataCube;

  public CubeData getDataCube() {
    return dataCube;
  }

  public boolean isEmpty() {
    return dataCube == null || dataCube.isEmpty();
  }

  @Override public String toString() {
    return "RatesEnvelope{" +
        "dataCube=" + dataCube +
        '}';
  }
}
