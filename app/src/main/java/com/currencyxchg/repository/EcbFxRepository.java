package com.currencyxchg.repository;

import com.currencyxchg.BuildConfig;
import com.currencyxchg.annotations.IOSched;
import com.currencyxchg.annotations.UISched;
import com.currencyxchg.net.EcbApi;
import com.currencyxchg.net.dto.CurrencyRate;
import com.currencyxchg.net.dto.RatesEnvelope;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Scheduler;
import rx.functions.Func1;

/**
 * Provides whole logic of communication with ECB API
 */
public class EcbFxRepository implements FxRepository {

  private final EcbApi api;
  private final @UISched Scheduler uiScheduler;
  private final @IOSched Scheduler ioScheduler;

  public EcbFxRepository(EcbApi api, Scheduler uiScheduler, Scheduler ioScheduler) {
    this.api = api;
    this.uiScheduler = uiScheduler;
    this.ioScheduler = ioScheduler;
  }

  /**
   * Fetch currencies from ECB server
   *
   * @return observable with map of currencies, key - 3-letters name of currency, value - object of
   * currency rate
   */
  @Override public Observable<Map<String, CurrencyRate>> fetchRates() {
    return rx.Observable.interval(0, BuildConfig.POLLING_PERIOD_IN_SECONDS, TimeUnit.SECONDS)
        .flatMap(new Func1<Long, rx.Observable<RatesEnvelope>>() {
          @Override public rx.Observable<RatesEnvelope> call(Long n) {
            return api.getEuroFxDaily()
                .retry(
                    3) // if we got an error (timeout, http error (4xx, 5xx)), allow it up to three attempts
                .subscribeOn(ioScheduler);
          }
        })
        .flatMap(new Func1<RatesEnvelope, rx.Observable<Map<String, CurrencyRate>>>() {
          @Override
          public rx.Observable<Map<String, CurrencyRate>> call(RatesEnvelope ratesEnvelope) {
            Map<String, CurrencyRate> result = new HashMap<>();
            if (!ratesEnvelope.isEmpty()) {
              result.putAll(ratesEnvelope.getDataCube().getCubeList().getRatesMap());
              // it's ECB and all currencies are normalized by euro, so we need to provide our pseudo-item with euro as 1.0
              result.put("EUR", new CurrencyRate("EUR", 1.0f));
            }

            return rx.Observable.just(result);
          }
        })
        .observeOn(uiScheduler);
  }
}
