package com.currencyxchg.repository;

import com.currencyxchg.net.dto.CurrencyRate;
import java.util.Map;
import rx.Observable;

/**
 * General interface for different Fx API
 *
 * It's good practice to make application extensible to provide different sources of data
 */
public interface FxRepository {

  Observable<Map<String, CurrencyRate>> fetchRates();
}
