package com.currencyxchg.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.currencyxchg.CurrencyExchangeApplication;
import com.currencyxchg.di.ActivityModule;
import com.currencyxchg.di.ActivitySubcomponent;
import com.currencyxchg.di.AppComponent;

/**
 * All activities must be inherited from BaseActivity
 */
public class BaseActivity extends AppCompatActivity {

  private ActivitySubcomponent component;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    AppComponent component = ((CurrencyExchangeApplication) getApplication()).getComponent();

    this.component = component.activitySubcomponent(new ActivityModule(this));
  }

  public ActivitySubcomponent getComponent() {
    return component;
  }
}
