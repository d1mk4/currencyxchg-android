package com.currencyxchg.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import com.currencyxchg.di.ActivitySubcomponent;
import com.currencyxchg.di.FragmentModule;
import com.currencyxchg.di.FragmentSubcomponent;

/**
 * All fragments must be inherited from BaseFragment
 */
public class BaseFragment extends Fragment {

  private FragmentSubcomponent component;

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    ActivitySubcomponent activityComponent = ((BaseActivity) getActivity()).getComponent();
    this.component = activityComponent.fragmentSubcomponent(new FragmentModule(this));
  }

  public FragmentSubcomponent getComponent() {
    return component;
  }
}
