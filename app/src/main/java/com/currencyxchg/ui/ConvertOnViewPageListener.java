package com.currencyxchg.ui;

import android.support.annotation.VisibleForTesting;
import android.support.v4.view.ViewPager;
import com.currencyxchg.net.dto.CurrencyRate;
import java.util.Map;

/**
 * Listener which provides currencies conversion by swiping pages in view pager
 */
public class ConvertOnViewPageListener implements ViewPager.OnPageChangeListener {

  private final CurrenciesPagerAdapter pagerAdapter;
  private final ViewPager srcPager;
  private final ViewPager dstPager;
  private Map<String, CurrencyRate> fxCurrencies;

  public ConvertOnViewPageListener(CurrenciesPagerAdapter pagerAdapter, ViewPager srcPager,
      ViewPager dstPager) {
    this.pagerAdapter = pagerAdapter;
    this.srcPager = srcPager;
    this.dstPager = dstPager;
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

  }

  @Override public void onPageSelected(int position) {
    if (fxCurrencies != null) {
      pagerAdapter.updateCurrencyAmount(fxCurrencies, srcPager.getCurrentItem(),
          dstPager.getCurrentItem());
    }
  }

  @Override public void onPageScrollStateChanged(int state) {

  }

  @VisibleForTesting public Map<String, CurrencyRate> getFxCurrencies() {
    return fxCurrencies;
  }

  public void setFxCurrencies(Map<String, CurrencyRate> fxCurrencies) {
    this.fxCurrencies = fxCurrencies;
  }
}
