package com.currencyxchg.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import com.currencyxchg.data.WalletData;
import com.currencyxchg.net.dto.CurrencyRate;
import com.currencyxchg.util.CurrencyUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * An adapter to show all wallets and which provides conversions of currencies
 */
public class CurrenciesPagerAdapter extends PagerAdapter {

  private final Context context;
  private final CurrencyView.OnChangeListener listener;
  private final List<WalletData> walletDatas = new ArrayList<>();

  private ViewGroup container;

  public CurrenciesPagerAdapter(Context context, CurrencyView.OnChangeListener listener) {
    this.context = context;
    this.listener = listener;
  }

  public int getItemPosition(Object object) {
    return POSITION_NONE;
  }

  public WalletData getItemAt(int index) {
    return walletDatas.get(index);
  }

  @Override public Object instantiateItem(ViewGroup container, int position) {
    this.container = container;
    CurrencyView view = createConverterView(position);
    container.addView(view);
    return view;
  }

  @NonNull protected CurrencyView createConverterView(int position) {
    WalletData walletData = walletDatas.get(position);
    return createConverterView(walletData);
  }

  @Override public int getCount() {
    return walletDatas.size();
  }

  @Override public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((View) object);
  }

  @Override public boolean isViewFromObject(View view, Object object) {
    return view == object;
  }

  @NonNull CurrencyView createConverterView(WalletData currency) {
    CurrencyView view = new CurrencyView(context);
    view.setWallet(currency);
    view.setOnChangeListener(listener);
    return view;
  }

  /**
   * @param wallets - wallets to update adapter
   */
  public void setWallets(List<WalletData> wallets) {
    this.walletDatas.clear();
    this.walletDatas.addAll(wallets);
    notifyDataSetChanged();
  }

  /**
   * Convert amount of money from one currency to another
   *
   * @param fxRates - map of currencies rates
   * @param srcPos - WalletData position in adapter
   * @param dstPos - WalletData position in adapter
   */
  public void updateCurrencyAmount(Map<String, CurrencyRate> fxRates, int srcPos, int dstPos) {
    WalletData srcWallet = walletDatas.get(srcPos);
    WalletData dstWallet = walletDatas.get(dstPos);

    CurrencyRate srcCurrencyRate = fxRates.get(srcWallet.getCurrency());
    CurrencyRate dstCurrencyRate = fxRates.get(dstWallet.getCurrency());

    dstWallet.setAmount(
        CurrencyUtils.convert(srcCurrencyRate, dstCurrencyRate, srcWallet.getAmount()));

    notifyDataSetChanged();
  }
}
