package com.currencyxchg.ui;

import android.content.Context;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import com.currencyxchg.R;
import com.currencyxchg.data.WalletData;
import com.currencyxchg.util.StringUtils;
import java.math.BigDecimal;

/**
 * View which represents currency and amount of money on wallet
 */
public class CurrencyView extends RelativeLayout {

  @BindView(R.id.currencyName) TextView currencyNameView;
  @BindView(R.id.currencyAmount) EditText currencyAmountView;
  private OnChangeListener onChangeListener;
  private WalletData walletData;

  public CurrencyView(Context context) {
    super(context);
    init(context);
  }

  public CurrencyView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public CurrencyView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context);
  }

  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
  public CurrencyView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    init(context);
  }

  @OnTextChanged(R.id.currencyAmount) void onTextChanged(CharSequence changed) {
    if (onChangeListener != null && !TextUtils.isEmpty(changed)) {
      walletData.setAmount(new BigDecimal(changed.toString()));
      onChangeListener.onChange();
    }
  }

  private void init(Context context) {
    inflate(context, R.layout.fragment_currency, this);

    ButterKnife.bind(this);
  }

  public void setWallet(WalletData walletData) {
    this.walletData = walletData;

    if (currencyNameView != null) {
      currencyNameView.setText(walletData.getCurrency());
      currencyAmountView.setText(StringUtils.formatMoney(walletData.getAmount().doubleValue()));
    }
  }

  public void setOnChangeListener(OnChangeListener onChangeListener) {
    this.onChangeListener = onChangeListener;
  }

  @Override protected Parcelable onSaveInstanceState() {
    Parcelable state = super.onSaveInstanceState();
    SavedState savedState = new SavedState(state);
    savedState.walletData = walletData;
    return savedState;
  }

  @Override protected void onRestoreInstanceState(Parcelable state) {
    SavedState savedState = (SavedState) state;
    super.onRestoreInstanceState(state);
    this.walletData = savedState.walletData;
  }

  /**
   * An interface to interact with fragments
   *
   * If amount of money has been changed then react to the fragments
   */
  public interface OnChangeListener {
    void onChange();
  }

  /**
   * just to provide saving and restoring data
   */
  static class SavedState extends BaseSavedState {
    public static final Parcelable.Creator<SavedState> CREATOR =
        new Parcelable.Creator<SavedState>() {
          public SavedState createFromParcel(Parcel in) {
            return new SavedState(in);
          }

          public SavedState[] newArray(int size) {
            return new SavedState[size];
          }
        };
    WalletData walletData;

    SavedState(Parcelable superState) {
      super(superState);
    }

    private SavedState(Parcel in) {
      super(in);
      walletData = WalletData.CREATOR.createFromParcel(in);
    }

    @Override public void writeToParcel(Parcel out, int flags) {
      super.writeToParcel(out, flags);
      out.writeParcelable(walletData, 0);
    }
  }
}
