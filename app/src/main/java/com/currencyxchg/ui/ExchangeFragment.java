package com.currencyxchg.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.currencyxchg.BuildConfig;
import com.currencyxchg.R;
import com.currencyxchg.data.WalletData;
import com.currencyxchg.net.dto.CurrencyRate;
import com.currencyxchg.repository.FxRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;

/**
 * The main fragment which contains whole logic of conversions
 */
public class ExchangeFragment extends BaseFragment {

  @Inject FxRepository fxRepository;

  @BindView(R.id.currenciesSrcPager) ViewPager currenciesSrcPager;
  @BindView(R.id.currenciesSrcIndicator) CircleIndicator currenciesSrcIndicator;
  @BindView(R.id.currenciesDstPager) ViewPager currenciesDstPager;
  @BindView(R.id.currenciesDstIndicator) CircleIndicator currenciesDstIndicator;

  private CurrenciesPagerAdapter srcCurrenciesPagerAdapter;
  private CurrenciesPagerAdapter dstCurrenciesPagerAdapter;

  private Map<String, CurrencyRate> fxCurrencies;
  private final CurrencyView.OnChangeListener srcListener = new CurrencyView.OnChangeListener() {
    @Override public void onChange() {
      if (fxCurrencies != null) {
        dstCurrenciesPagerAdapter.updateCurrencyAmount(fxCurrencies,
            currenciesSrcPager.getCurrentItem(), currenciesDstPager.getCurrentItem());
      } else {
        Toast.makeText(getContext(), R.string.fx_currencies_not_loaded, Toast.LENGTH_SHORT).show();
      }
    }
  };

  private final CurrencyView.OnChangeListener dstListener = new CurrencyView.OnChangeListener() {
    @Override public void onChange() {
      if (fxCurrencies != null) {
        srcCurrenciesPagerAdapter.updateCurrencyAmount(fxCurrencies,
            currenciesDstPager.getCurrentItem(), currenciesSrcPager.getCurrentItem());
      } else {
        Toast.makeText(getContext(), R.string.fx_currencies_not_loaded, Toast.LENGTH_SHORT).show();
      }
    }
  };

  private Subscription rxSubscription;
  private Unbinder unbinder;
  private ConvertOnViewPageListener srcPageListener;
  private ConvertOnViewPageListener dstPageListener;

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_exchange, container, false);
    unbinder = ButterKnife.bind(this, v);
    return v;
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    getComponent().inject(this);

    srcCurrenciesPagerAdapter = new CurrenciesPagerAdapter(getContext(), srcListener);
    dstCurrenciesPagerAdapter = new CurrenciesPagerAdapter(getContext(), dstListener);

    List<WalletData> wallets = new ArrayList<>();

    for (String currencyName : BuildConfig.CURRENCIES) {
      wallets.add(new WalletData(currencyName));
    }

    srcCurrenciesPagerAdapter.setWallets(wallets);
    dstCurrenciesPagerAdapter.setWallets(wallets);

    currenciesSrcPager.setAdapter(srcCurrenciesPagerAdapter);
    currenciesSrcIndicator.setViewPager(currenciesSrcPager);

    currenciesDstPager.setAdapter(dstCurrenciesPagerAdapter);
    currenciesDstIndicator.setViewPager(currenciesDstPager);

    srcPageListener = new ConvertOnViewPageListener(srcCurrenciesPagerAdapter, currenciesDstPager,
        currenciesSrcPager);
    dstPageListener = new ConvertOnViewPageListener(dstCurrenciesPagerAdapter, currenciesSrcPager,
        currenciesDstPager);

    currenciesSrcPager.addOnPageChangeListener(srcPageListener);
    currenciesDstPager.addOnPageChangeListener(dstPageListener);
  }

  @Override public void onDestroyView() {
    super.onDestroyView();

    if (srcPageListener != null) {
      currenciesSrcPager.removeOnPageChangeListener(srcPageListener);
    }

    if (dstPageListener != null) {
      currenciesDstPager.removeOnPageChangeListener(dstPageListener);
    }

    // do unbind just to avoid memory leaks
    if (unbinder != null) {
      unbinder.unbind();
    }
  }

  @Override public void onResume() {
    super.onResume();

    rxSubscription = fetchFxRates();
  }

  @Override public void onPause() {
    super.onPause();

    // always check subscriptions
    if (rxSubscription != null && !rxSubscription.isUnsubscribed()) {
      rxSubscription.unsubscribe();
    }
  }

  /**
   * Fetch fx rates from provided repository (in our case it's ECB)
   *
   * @return subscription to rx flow
   */
  private Subscription fetchFxRates() {
    return fxRepository.fetchRates().subscribe(new Subscriber<Map<String, CurrencyRate>>() {
      @Override public void onCompleted() {

      }

      @Override public void onError(Throwable e) {
        if (e instanceof HttpException) {
          // We had non-2XX http error
          Toast.makeText(getContext(), "http: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT)
              .show();
        } else if (e instanceof IOException) {
          // A network or conversion error happened
          Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
      }

      @Override public void onNext(Map<String, CurrencyRate> result) {
        fxCurrencies = result;

        srcPageListener.setFxCurrencies(fxCurrencies);
        dstPageListener.setFxCurrencies(fxCurrencies);
      }
    });
  }

  @VisibleForTesting
  public Map<String, CurrencyRate> getFxCurrencies() {
    return fxCurrencies;
  }

  @VisibleForTesting public ConvertOnViewPageListener getSrcPageListener() {
    return srcPageListener;
  }

  @VisibleForTesting public ConvertOnViewPageListener getDstPageListener() {
    return dstPageListener;
  }

  @VisibleForTesting public CurrenciesPagerAdapter getSrcCurrenciesPagerAdapter() {
    return srcCurrenciesPagerAdapter;
  }

  @VisibleForTesting public CurrenciesPagerAdapter getDstCurrenciesPagerAdapter() {
    return dstCurrenciesPagerAdapter;
  }
}
