package com.currencyxchg.ui;

import android.os.Bundle;
import com.currencyxchg.R;

/**
 * I use main activity as a container for all fragments
 */
public class MainActivity extends BaseActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }
}
