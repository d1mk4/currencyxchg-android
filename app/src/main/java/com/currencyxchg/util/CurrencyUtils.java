package com.currencyxchg.util;

import com.currencyxchg.net.dto.CurrencyRate;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

public class CurrencyUtils {

  /**
   * convert currency from one to another
   *
   * @param srcCurrency currency in which value
   * @param dstCurrency currency in which we want to convert our money
   * @param value amount of money
   * @return converted amount of money
   */
  public static BigDecimal convert(CurrencyRate srcCurrency, CurrencyRate dstCurrency,
      BigDecimal value) {
    BigDecimal srcRate = new BigDecimal(srcCurrency.getRate());
    BigDecimal dstRate = new BigDecimal(dstCurrency.getRate());

    int rounding = Currency.getInstance(dstCurrency.getCurrency()).getDefaultFractionDigits();

    return value.multiply(dstRate).divide(srcRate, rounding, RoundingMode.HALF_DOWN);
  }
}
