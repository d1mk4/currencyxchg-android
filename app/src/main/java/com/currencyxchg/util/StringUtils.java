package com.currencyxchg.util;

import java.util.Locale;

public class StringUtils {

  /**
   * always format money as 0.00
   *
   * @param amount - amount to be converted in String
   * @return formatted string in format 0.00
   */
  public static String formatMoney(float amount) {
    return String.format(Locale.US, "%.2f", amount);
  }

  /**
   *
   * always format money as 0.00
   *
   * @param amount - amount to be converted in String
   * @return formatted string in format 0.00
   */
  public static String formatMoney(double amount) {
    return String.format(Locale.US, "%.2f", amount);
  }
}
