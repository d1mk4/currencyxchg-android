package com.currencyxchg;

import android.app.Application;
import com.currencyxchg.annotations.IOSched;
import com.currencyxchg.annotations.UISched;
import com.currencyxchg.di.AppComponent;
import com.currencyxchg.di.AppModule;
import com.currencyxchg.di.DaggerAppComponent;
import com.currencyxchg.net.EcbApi;
import com.currencyxchg.net.dto.CurrencyRate;
import com.currencyxchg.repository.FxRepository;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.robolectric.TestLifecycleApplication;
import rx.Observable;
import rx.Scheduler;
import rx.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestApp extends CurrencyExchangeApplication implements TestLifecycleApplication {

  public static class TestAppModule extends AppModule {

    public TestAppModule(Application application) {
      super(application);
    }

    @Override public Application provideApplication() {
      return super.provideApplication();
    }

    @Override public Scheduler provideIOScheduler() {
      return Schedulers.immediate();
    }

    @Override public Scheduler provideUIScheduler() {
      return Schedulers.immediate();
    }

    @Override public EcbApi provideApi(Application application) {
      return mock(EcbApi.class);
    }

    @Override public FxRepository provideFxRepository(EcbApi api, @UISched Scheduler uiScheduler,
        @IOSched Scheduler ioScheduler) {

      FxRepository repository = mock(FxRepository.class);

      Map<String, CurrencyRate> currencyRates = new HashMap<>();

      currencyRates.put("EUR", new CurrencyRate("EUR", 1.0f));
      currencyRates.put("USD", new CurrencyRate("USD", 2.0f));
      currencyRates.put("GBP", new CurrencyRate("GBP", 3.0f));

      Observable<Map<String, CurrencyRate>> currencyRatesRx = Observable.just(currencyRates)
          .observeOn(Schedulers.immediate())
          .subscribeOn(Schedulers.immediate());

      when(repository.fetchRates()).thenReturn(currencyRatesRx);

      return repository;
    }
  }

  @Override protected AppComponent createComponent() {
    AppModule module = new TestAppModule(this);
    return DaggerAppComponent.builder().appModule(module).build();
  }

  @Override public void beforeTest(Method method) {

  }

  @Override public void prepareTest(Object test) {

  }

  @Override public void afterTest(Method method) {

  }
}
