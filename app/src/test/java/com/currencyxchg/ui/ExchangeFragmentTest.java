package com.currencyxchg.ui;

import com.currencyxchg.BuildConfig;
import com.currencyxchg.R;
import com.currencyxchg.TestApp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, application = TestApp.class)
public class ExchangeFragmentTest {

  ExchangeFragment fragment;

  @Before public void setUp() throws Exception {
    fragment = new ExchangeFragment();
    SupportFragmentTestUtil.startVisibleFragment(fragment, MainActivity.class, R.id.container);
  }

  @After public void tearDown() throws Exception {
    fragment = null;
  }

  @Test public void testFetchFxCurrencies() throws Exception {

    Assert.assertNotNull(fragment.getFxCurrencies());

    Assert.assertNotNull(fragment.getSrcPageListener().getFxCurrencies());
    Assert.assertNotNull(fragment.getDstPageListener().getFxCurrencies());

    Assert.assertNotNull(BuildConfig.CURRENCIES);
    Assert.assertTrue(BuildConfig.CURRENCIES.length > 0);

    Assert.assertEquals(BuildConfig.CURRENCIES.length,
        fragment.getSrcCurrenciesPagerAdapter().getCount());
    Assert.assertEquals(BuildConfig.CURRENCIES.length,
        fragment.getDstCurrenciesPagerAdapter().getCount());

    for (int i = 0; i < BuildConfig.CURRENCIES.length; ++i) {
      Assert.assertEquals(BuildConfig.CURRENCIES[i],
          fragment.getSrcCurrenciesPagerAdapter().getItemAt(i).getCurrency());
    }

    for (int i = 0; i < BuildConfig.CURRENCIES.length; ++i) {
      Assert.assertEquals(BuildConfig.CURRENCIES[i],
          fragment.getDstCurrenciesPagerAdapter().getItemAt(i).getCurrency());
    }
  }
}
