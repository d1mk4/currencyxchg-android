package com.currencyxchg.util;

import com.currencyxchg.net.dto.CurrencyRate;
import java.math.BigDecimal;
import junit.framework.Assert;
import org.junit.Test;

public class CurrencyUtilsTest {

  public static final CurrencyRate USD_RATE = new CurrencyRate("USD", 1.12201f);
  public static final CurrencyRate GBP_RATE = new CurrencyRate("GBP", 0.9f);
  public static final CurrencyRate EUR_RATE = new CurrencyRate("EUR", 1.0f);

  public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

  public static final BigDecimal ONE_BILLION = new BigDecimal(1000000000);

  @Test
  public void testUSD_EUR_ONE_HUNDRED() throws Exception {
    Assert.assertEquals("89.13", CurrencyUtils.convert(USD_RATE, EUR_RATE, ONE_HUNDRED).toString());
    Assert.assertEquals("112.20", CurrencyUtils.convert(EUR_RATE, USD_RATE, ONE_HUNDRED).toString());
  }

  @Test
  public void testGBP_EUR_ONE_HUNDRED() throws Exception {
    Assert.assertEquals("111.11", CurrencyUtils.convert(GBP_RATE, EUR_RATE, ONE_HUNDRED).toString());
    Assert.assertEquals("90.00", CurrencyUtils.convert(EUR_RATE, GBP_RATE, ONE_HUNDRED).toString());
  }

  @Test public void testGBP_USD_ONE_HUNDRED() throws Exception {
    Assert.assertEquals("124.67", CurrencyUtils.convert(GBP_RATE, USD_RATE, ONE_HUNDRED).toString());
    Assert.assertEquals("80.21", CurrencyUtils.convert(USD_RATE, GBP_RATE, ONE_HUNDRED).toString());
  }

  @Test public void testUSD_EUR_ZERO() throws Exception {
    Assert.assertEquals("0.00", CurrencyUtils.convert(USD_RATE, EUR_RATE, BigDecimal.ZERO).toString());
    Assert.assertEquals("0.00", CurrencyUtils.convert(EUR_RATE, USD_RATE, BigDecimal.ZERO).toString());
  }

  @Test public void testUSD_EUR_ONE() throws Exception {
    Assert.assertEquals("0.89", CurrencyUtils.convert(USD_RATE, EUR_RATE, BigDecimal.ONE).toString());
    Assert.assertEquals("1.12", CurrencyUtils.convert(EUR_RATE, USD_RATE, BigDecimal.ONE).toString());
  }


  @Test public void testGBP_EUR_ZERO() throws Exception {
    Assert.assertEquals("0.00", CurrencyUtils.convert(GBP_RATE, EUR_RATE, BigDecimal.ZERO).toString());
    Assert.assertEquals("0.00", CurrencyUtils.convert(EUR_RATE, GBP_RATE, BigDecimal.ZERO).toString());
  }

  @Test public void testGBP_EUR_ONE() throws Exception {
    Assert.assertEquals("1.11", CurrencyUtils.convert(GBP_RATE, EUR_RATE, BigDecimal.ONE).toString());
    Assert.assertEquals("0.90", CurrencyUtils.convert(EUR_RATE, GBP_RATE, BigDecimal.ONE).toString());
  }

  @Test public void testGBP_USD_ZERO() throws Exception {
    Assert.assertEquals("0.00", CurrencyUtils.convert(GBP_RATE, USD_RATE, BigDecimal.ZERO).toString());
    Assert.assertEquals("0.00", CurrencyUtils.convert(USD_RATE, GBP_RATE, BigDecimal.ZERO).toString());
  }

  @Test
  public void testGBP_USD_ONE() throws Exception {
    Assert.assertEquals("1.25", CurrencyUtils.convert(GBP_RATE, USD_RATE, BigDecimal.ONE).toString());
    Assert.assertEquals("0.80", CurrencyUtils.convert(USD_RATE, GBP_RATE, BigDecimal.ONE).toString());
  }

  @Test
  public void testUSD_EUR_ONE_BILLION() throws Exception {
    Assert.assertEquals("891257659.55", CurrencyUtils.convert(USD_RATE, EUR_RATE, ONE_BILLION).toString());
    Assert.assertEquals("1122009992.60", CurrencyUtils.convert(EUR_RATE, USD_RATE, ONE_BILLION).toString());
  }

  @Test
  public void testGBP_EUR_ONE_BILLION() throws Exception {
    Assert.assertEquals("1111111140.55", CurrencyUtils.convert(GBP_RATE, EUR_RATE, ONE_BILLION).toString());
    Assert.assertEquals("899999976.16", CurrencyUtils.convert(EUR_RATE, GBP_RATE, ONE_BILLION).toString());
  }

  @Test public void testGBP_USD_ONE_BILLION() throws Exception {
    Assert.assertEquals("1246677802.58", CurrencyUtils.convert(GBP_RATE, USD_RATE, ONE_BILLION).toString());
    Assert.assertEquals("802131872.35", CurrencyUtils.convert(USD_RATE, GBP_RATE, ONE_BILLION).toString());
  }
}
