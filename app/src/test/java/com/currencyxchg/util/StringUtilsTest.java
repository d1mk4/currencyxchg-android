package com.currencyxchg.util;

import junit.framework.Assert;
import org.junit.Test;

public class StringUtilsTest {

  @Test public void testFormatMoney() throws Exception {
    Assert.assertEquals("1.00", StringUtils.formatMoney(1f));
    Assert.assertEquals("1.00", StringUtils.formatMoney(1d));

    Assert.assertEquals("0.53", StringUtils.formatMoney(0.5333333f));
    Assert.assertEquals("0.53", StringUtils.formatMoney(0.5333333d));
  }
}
